#include <EEPROM.h>
#include <RTClib.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>


#define RELAY_PIN 7
#define MAN_PIN 4
#define MENU_PIN 5
#define PLUS_PIN 3
#define MENO_PIN 2

#define DEBOUNCE_DELAY 20
#define DELAY_TEMP 1
#define ANTIFREEZE_TEMP 5
#define INTERVALLO_LETTURA 15000
#define BACKLIGHT_TIME 10000
#define TIME_INTERVAL 30000

#define MAN 0
#define AUT 1
#define GELO 2


char* days[]{"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
struct prog{
    byte oraAcc;
    byte minAcc;
    byte oraSpegn;
    byte minSpegn;
};
struct prog programmi[3];

// LiquidCrystal_I2C lcd(0x27 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);
LiquidCrystal_I2C lcd(0x27,20,4);
RTC_DS1307 rtc;

//TODO: change the defautl start temperature
float tempAcc=25; //start temp
float temp;
byte manuale=MAN;
byte menuMod=0;
byte menuClockMod=0;

//variabili d'appoggio
bool previousPMeno=LOW;
bool previousPPiu=LOW;
bool previousPMan=LOW;
bool previousPMenu=LOW;
long tempoM;

long tempoLeggiTemp;
long tempoLeggiOra;
long tempoBacklight;
bool backlightOn=true;
byte progIndex=0;
char str[17];
bool isOn=false;
bool setBacklightOff=true;


void setup() {
    Serial.begin(9600);
    Wire.begin();
    pinMode(RELAY_PIN, OUTPUT);
    pinMode(MAN_PIN, INPUT);
    pinMode(MENU_PIN, INPUT);
    pinMode(PLUS_PIN, INPUT);
    pinMode(MENO_PIN, INPUT);
    
    lcd.begin(20,4);
    //lcd.init();
    lcd.backlight();  //start backlight
    
    rtc.begin();
    //rtc.adjust(DateTime(2019, 12, 25, 23, 51, 55)); 
    
    leggiTemperatura();
    stampaTempAccensione(tempAcc);
    stampaAM();
    accendiSpegni();
    stampaDataOra();
    tempoBacklight=millis();

    int address=0;
    for(byte i=0; i<3; i++){
        programmi[i]= EEPROM.get(address, programmi[i]);
        address+=4;
    }  
}

void loop() {
    if(millis()-tempoLeggiTemp>INTERVALLO_LETTURA){
        tempoLeggiTemp=millis();
        leggiTemperatura();
        accendiSpegni();/*ON OFF control with start or antifreeze tempetature */
    }

    if(menuMod==0 && millis()-tempoLeggiOra>TIME_INTERVAL){
       stampaDataOra();
    }

    // gestione della backlight
    if(backlightOn && millis()-tempoBacklight>BACKLIGHT_TIME && setBacklightOff){
        lcd.noBacklight();
        backlightOn=false;
    }
    

    /******************************* manual-auto ********************************/
    bool leggiPMan=debounce(MAN_PIN,previousPMan);
    if(leggiPMan==LOW && previousPMan==HIGH){
        tempoBacklight=millis();
        if(!backlightOn){
            lcd.backlight();
            backlightOn=true;
        }else{
            if(menuMod==0 && menuClockMod==0){
                manuale=++manuale%3;
                stampaAM();
                accendiSpegni();
            }else {
                menuMod=0;
                menuClockMod=0;
                aggiustaDataErrata();
                writeProg();
                stampaMenuModZero();
            }
        }
    }
    previousPMan=leggiPMan;


    /*******************************menu section******************************/
    bool leggiPMenu=debounce(MENU_PIN, previousPMenu);
    if(leggiPMenu== HIGH && previousPMenu==LOW){
        tempoM=millis();
    }
    if(leggiPMenu== LOW && previousPMenu==HIGH){
        lcd.setCursor(0,2);
        tempoBacklight=millis();
        if(!backlightOn){
            lcd.backlight();
            backlightOn=true;
        }else{
            if((millis()-tempoM)>2000){
                if(menuMod==0){
                    menuClockMod=(++menuClockMod)%6;
                    menuClock();
                }
            }else{
                if(menuClockMod==0){
                    menuMod=(++menuMod)%15;
                }
                if(menuMod==0){
                    if(menuClockMod!=0){
                        menuClockMod=(++menuClockMod)%6;
                        menuClock();
                    }else{
                        writeProg();
                        stampaMenuModZero();
                    }
                }else if(menuMod==1){
                    lcd.clear();
                    lcd.setCursor(0,1);
                    lcd.print("Ins ST ");
                    lcd.print(tempAcc,1);//////////////////////////////////////
                }else if(menuMod==14){
                    lcd.clear();
                    lcd.setCursor(0,1);
                    lcd.print("Turn Off Light:");  
                    if(setBacklightOff){
                       lcd.print("ON ");
                    }else{
                       lcd.print("OFF");
                    }
                }else if((menuMod-2)%4==0){
                    lcd.setCursor(0,0);
                    progIndex=(menuMod-2)/4;
                    sprintf(str, "P%d %02d:%02d-%02d:%02d", progIndex+1, programmi[progIndex].oraAcc, programmi[progIndex].minAcc, programmi[progIndex].oraSpegn, programmi[progIndex].minSpegn);
                    lcd.print(str);
                    lcd.setCursor(0,1);
                    lcd.print("Mod start Hr "); 
                }else if((menuMod-2)%4==1){
                    lcd.setCursor(0,1);
                    lcd.print("Mod start Mn ");
                }else if((menuMod-2)%4==2){
                     if(programmi[progIndex].oraAcc >programmi[progIndex].oraSpegn || (programmi[progIndex].oraAcc==programmi[progIndex].oraSpegn && programmi[progIndex].minAcc> programmi[progIndex].minSpegn)){
                        progIndex=(menuMod-2)/4;
                        programmi[progIndex].oraSpegn=programmi[progIndex].oraAcc;
                        programmi[progIndex].minSpegn= programmi[progIndex].minAcc;
                        sprintf(str, "P%d %02d:%02d-%02d:%02d -", progIndex+1, programmi[progIndex].oraAcc, programmi[progIndex].minAcc, programmi[progIndex].oraSpegn, programmi[progIndex].minSpegn);
                        lcd.setCursor(0,0);
                        lcd.print(str);
                    }
                    lcd.setCursor(0,1);
                    lcd.print("Mod end Hr   ");
                }else if((menuMod-2)%4==3){
                    lcd.setCursor(0,1);
                    lcd.print("Mod end Mn  ");
                }

                if(menuMod!=0){
                    lcd.setCursor(1,3);
                    lcd.print("M/A to exit   ");                }
            }
        }//end else di backlight off
    }
    previousPMenu=leggiPMenu;

    
    /************************button +**************************/
    bool leggiPPiu=debounce(PLUS_PIN, previousPPiu );
    if(leggiPPiu == LOW && previousPPiu== HIGH){
        tempoBacklight=millis();
        if(!backlightOn){
            lcd.backlight();
            backlightOn=true;
        }else if(menuMod==1){
            tempAcc+=0.5;
            lcd.setCursor(0,1);
            lcd.print("Ins ST ");
            lcd.print(tempAcc,1);///////////////////
        }else if(menuMod==14){
            lcd.setCursor(15,1);
            if(setBacklightOff){
                lcd.print("OFF");
                setBacklightOff=false;
            }else{
                lcd.print("ON ");
                setBacklightOff=true;
            }
        }else if((menuMod-2)%4==0){
            programmi[progIndex].oraAcc++;
            if(programmi[progIndex].oraAcc==24){ //sta fatto con l'if e non col % perchÃ¨ coi negativi sul bottone - da problemi
                programmi[progIndex].oraAcc=0;
            } 
        }else if((menuMod-2)%4==1){
            programmi[progIndex].minAcc+=5;
            if(programmi[progIndex].minAcc==60){ 
                programmi[progIndex].minAcc=0;
            } 
        }else if((menuMod-2)%4==2){
            programmi[progIndex].oraSpegn++;
            if(programmi[progIndex].oraSpegn==24){ 
                programmi[progIndex].oraSpegn=0;
            } 
        }else if((menuMod-2)%4==3){
            programmi[progIndex].minSpegn+=5;
            if(programmi[progIndex].minSpegn==60){ 
                programmi[progIndex].minSpegn=0;
            } 
        }else if(menuClockMod!=0){
            DateTime now = rtc.now();
            if(menuClockMod==1){
                rtc.adjust(DateTime(now.year(), now.month(), now.day()+1>31? 1 : now.day()+1, now.hour(), now.minute(), now.second()));
            }else if(menuClockMod==2){
                rtc.adjust(DateTime(now.year(), now.month()+1>=13?1:now.month()+1, now.day(), now.hour(), now.minute(), now.second()));
            }else if(menuClockMod==3){
                rtc.adjust(DateTime(now.year()+1, now.month(), now.day(), now.hour(), now.minute(), now.second()));
            }else if(menuClockMod==4){
                rtc.adjust(DateTime(now.year(), now.month(), now.day(), now.hour()+1>=24? 0: now.hour()+1, now.minute(), now.second()));
            }else if(menuClockMod==5){
                rtc.adjust(DateTime(now.year(), now.month(), now.day(), now.hour(), now.minute()+1>=60?0:now.minute()+1 , now.second()));
            }
            lcd.setCursor(0,0);
            stampaDataOra();
        }
       
      
        if(menuMod>=2 && menuMod<=13){
            lcd.setCursor(0,0);
            sprintf(str, "P%d %02d:%02d-%02d:%02d", progIndex+1, programmi[progIndex].oraAcc, programmi[progIndex].minAcc, programmi[progIndex].oraSpegn, programmi[progIndex].minSpegn);
            lcd.print(str);
        }
    }
    previousPPiu=leggiPPiu;
       

    /***************************bottone -******************************/
    bool leggiPMeno=debounce(MENO_PIN, previousPMeno);
    if(leggiPMeno == LOW && previousPMeno== HIGH){
        tempoBacklight=millis();  
        if(!backlightOn){
            lcd.backlight();
            backlightOn=true;
        }else if(menuMod==1){
            tempAcc-=0.5;
            lcd.setCursor(0,1);
            lcd.print("Ins ST ");
            lcd.print(tempAcc,1);///////////////////////////
        }else if(menuMod==14){
            lcd.setCursor(15,1);
            if(setBacklightOff){
                lcd.print("OFF");
                setBacklightOff=false;
            }else{
                lcd.print("ON ");
                setBacklightOff=true;
            }
        }else if((menuMod-2)%4==0){
                programmi[progIndex].oraAcc--;
                if(programmi[progIndex].oraAcc>200){ //il byte Ã¨ senza segno(si, nmon Ã¨ proprio 200)
                  programmi[progIndex].oraAcc=23;
                } 
        }else if((menuMod-2)%4==1){
            programmi[progIndex].minAcc-=5;
            if(programmi[progIndex].minAcc>200){ 
                programmi[progIndex].minAcc=55;
            } 
        }else if((menuMod-2)%4==2){
            programmi[progIndex].oraSpegn--;
            if(programmi[progIndex].oraSpegn>200){ 
                programmi[progIndex].oraSpegn=23;
            } 
        }else if((menuMod-2)%4==3){
            programmi[progIndex].minSpegn-=5;
            if(programmi[progIndex].minSpegn>200){ 
                programmi[progIndex].minSpegn=55;
            } 
        }else if(menuClockMod!=0){
            DateTime now = rtc.now();
            if(menuClockMod==1){
                rtc.adjust(DateTime(now.year(), now.month(), now.day()-1<0 ? 31 : now.day()-1, now.hour(), now.minute(), now.second()));
            }else if(menuClockMod==2){
                rtc.adjust(DateTime(now.year(), now.month()-1<=0? 12: now.month()-1, now.day(), now.hour(), now.minute(), now.second()));
            }else if(menuClockMod==3){
                rtc.adjust(DateTime(now.year()-1, now.month(), now.day(), now.hour(), now.minute(), now.second()));
            }else if(menuClockMod==4){
                rtc.adjust(DateTime(now.year(), now.month(), now.day(), now.hour()-1<0? 23: now.hour()-1, now.minute(), now.second()));
            }else if(menuClockMod==5){
                rtc.adjust(DateTime(now.year(), now.month(), now.day(), now.hour(), now.minute()-1<0? 59:now.minute()-1 , now.second()));
            }
            lcd.setCursor(0,0);
            stampaDataOra();
        }
        if(menuMod>=2 && menuMod<=13){
            lcd.setCursor(0,0);
            sprintf(str, "P%d %02d:%02d-%02d:%02d", progIndex+1, programmi[progIndex].oraAcc, programmi[progIndex].minAcc, programmi[progIndex].oraSpegn, programmi[progIndex].minSpegn);
            lcd.print(str);
        }
    }
    previousPMeno=leggiPMeno;
         
}
///////////////////////////////////////// UTILITY FUNCTIONS /////////////////////////////////////////////
/**************************************** temperature management*********************************************/
void leggiTemperatura(){
    temp=(float)analogRead(A0);
    temp*= 0.48875; //lm35
    //temp=temp/2.048
    //temp=((temp * 0.00488) - 0.5) / 0.01; //tmp36
    if(menuMod==0 && menuClockMod==0){
        lcd.setCursor(0,1);
        lcd.print("T ");
        lcd.print(temp,1);  //problema di formatt dei float. non tiene %f nella sprintf
        lcd.setCursor(6,1);  
        lcd.print("C");
    }
}

void stampaTempAccensione(float tempSoglia){
    lcd.setCursor(8,1);  
    lcd.print("sT ");
    lcd.print(tempSoglia,1);
    lcd.print("C ");
}

void stampaAM(){
    if(manuale==MAN){
        lcd.setCursor(17,1);
        lcd.print("MAN");
        stampaTempAccensione(tempAcc);
    }else if(manuale==AUT){
        lcd.setCursor(17,1);
        lcd.print("AUT");
    }else if(manuale== GELO){
        lcd.setCursor(17,1);
        lcd.print("FRZ");
        stampaTempAccensione(ANTIFREEZE_TEMP);
    }
}

void accendiSpegni(){
    if(manuale== MAN){
        confrontoTemp(tempAcc);
        lcd.setCursor(0,2);
        lcd.print("               ");
    }else if(manuale==GELO){
        confrontoTemp(ANTIFREEZE_TEMP);
        lcd.setCursor(0,2);
        lcd.print("               ");
    }else if(manuale==AUT){
       /* DateTime now = rtc.now();
        int ore=now.hour(); 
        int minuti= now.minute();*/
        byte i=0;
        DateTime now= rtc.now();
        long nowUnixTime=now.unixtime();
        byte printIndex=255;
        for( i=0; i<3;i++){
            long oraAcc=DateTime(now.year(), now.month(), now.day(), programmi[i].oraAcc,programmi[i].minAcc,0).unixtime();
            long oraSpegn=DateTime(now.year(), now.month(), now.day(), programmi[i].oraSpegn,programmi[i].minSpegn,0).unixtime();
            Serial.print(oraAcc);
            Serial.print("\n TS");
            Serial.print(oraSpegn);
            Serial.print("\n now");
            Serial.print(nowUnixTime);
            Serial.print("\n");
            if(nowUnixTime>=oraAcc && nowUnixTime<oraSpegn){
                confrontoTemp(tempAcc);
                printIndex=i;
                break;
            }else if(nowUnixTime<oraAcc && printIndex==255){
                printIndex=i;
            }        
            /*if(ore>= programmi[i].oraAcc && minuti>=programmi[i].minAcc && ore<= programmi[i].oraSpegn && minuti<programmi[i].minSpegn){
                confrontoTemp(tempAcc);
                break;
            }*/
        }
        if(menuMod==0){
            lcd.setCursor(0,2);
            sprintf(str, "P%d %02d:%02d-%02d:%02d", printIndex+1, programmi[printIndex].oraAcc, programmi[printIndex].minAcc, programmi[printIndex].oraSpegn, programmi[printIndex].minSpegn);
            lcd.print(str);
        }
        if(i==3){
            digitalWrite(RELAY_PIN, LOW);
            if(menuMod==0){
                lcd.setCursor(4,3);
                lcd.print("Term. spenti  ");
            }
        }
    }   
  
}

void confrontoTemp(float tempSoglia){
    if(temp<tempSoglia-DELAY_TEMP){
        digitalWrite(RELAY_PIN, HIGH);
        if(menuMod==0 && menuClockMod==0){
            lcd.setCursor(4,3);
            lcd.print("Heat  on      ");
            isOn=true;
            
        }
    }else if(temp>tempSoglia){
        digitalWrite(RELAY_PIN, LOW);
        if(menuMod==0 && menuClockMod==0){
            lcd.setCursor(4,3);
            lcd.print("Heat  off     ");
            isOn=false;
        }
    }else  if(menuMod==0 && menuClockMod==0){
        lcd.setCursor(4,3);
        if(isOn){
            lcd.print("Heat  on      ");
        }else{
            lcd.print("Heat  off     ");
        }
    }
}

/*****************************************menu management***************************************************/
void stampaMenuModZero(){
    lcd.clear();
    leggiTemperatura();
    stampaDataOra();
    if(manuale== MAN || manuale==AUT){
        stampaTempAccensione(tempAcc);
    }else{
        stampaTempAccensione(ANTIFREEZE_TEMP);
    }
    stampaAM();
    accendiSpegni();
}

void menuClock(){
    if(menuClockMod==0){
        aggiustaDataErrata();
        stampaMenuModZero();
    }else if(menuClockMod==1){
        lcd.clear();
        stampaDataOra();
        lcd.setCursor(0,1);
        lcd.print("Ins Day   ");
    }else if(menuClockMod==2){
        lcd.clear();
        stampaDataOra();
        lcd.setCursor(0,1);
        lcd.print("Ins Month");
    }else if(menuClockMod==3){
        lcd.clear();
        stampaDataOra();
        lcd.setCursor(0,1);
        lcd.print("Ins Year");
    }else if(menuClockMod==4){
        lcd.clear();
        stampaDataOra();
        lcd.setCursor(0,1);
        lcd.print("Ins Hour");
    }else if(menuClockMod==5){
        lcd.clear();
        stampaDataOra();
        lcd.setCursor(0,1);
        lcd.print("Ins Minute");
    }

    if(menuClockMod!=0){
        lcd.setCursor(1,3);
        lcd.print("M/A to Exit   ");
    }
}
/**************************************other***************************************/
void stampaDataOra(){
    DateTime now = rtc.now();
    char ora[20];
    sprintf(ora, "%s %02d/%02d/%02d %02d:%02d" , days[now.dayOfTheWeek()], now.day(), now.month(), now.year()%100, now.hour(), now.minute());
    lcd.setCursor(1, 0);
    lcd.print(ora);
    //trOra = millis();
}

void aggiustaDataErrata(){
    DateTime now= rtc.now();
    DateTime nuovoD= DateTime(now.unixtime());
    rtc.adjust(DateTime(nuovoD.year(), nuovoD.month(), nuovoD.day(), now.hour(), now.minute(), now.second()));
}

void writeProg(){
    int address=0;
    for(byte i=0; i<3; i++){
        if(programmi[i].oraAcc != EEPROM.read(address)){
            EEPROM.write(address, programmi[i].oraAcc);
        }
        address++;
        if(programmi[i].minAcc !=EEPROM.read(address)){
            EEPROM.write(address, programmi[i].minAcc);
        }
        address++;
        if( programmi[i].oraSpegn !=EEPROM.read(address)){
            EEPROM.write(address, programmi[i].oraSpegn);
        }
        address++;
        if( programmi[i].minSpegn != EEPROM.read(address)){
            EEPROM.write(address, programmi[i].minSpegn);
        }
        address++;
    }  
}

bool debounce(int pin, bool prevMesuredState){
    bool state;
    bool previousState;
    previousState=digitalRead(pin);
    if(previousState==LOW && prevMesuredState==LOW){
        return 0; 
    }
    for(int i=0; i<DEBOUNCE_DELAY; i++){
        delay(1);
        state=digitalRead(pin);
        if(state!=previousState){
            i=0;
            previousState=state;
        }  
    }
    return state;
}