# Chronotermostat
This project let you build tour own chronotermostat in Arduino

## Arduino project

![scheme!](./Scheme.png)

Components required:

* Arduino nano
* Relay board 
* I2C lcd module
* RTC module
* LM35
* 4 Buttons

## The code 
To use this  project download the `.ino` script and import it in you Arduino project. 
The required library are:

* EEPROM
* Wire
* RTClib (by Adafruit version 1.3.3)
* LiquidCrystal_I2C.h (by Frank de Bradander version 1.1.2)

## Usage
As you can see in the project scheme you have four buttons. 

* AUT/MAN
* MENU
* PLUS
* MINUS

To save power after `TEMPO_BACKLIGHT` millis the LCD backlight will turn off. Press any button to turn it on.


Pressing AUT/MAN button you can switch between automatic, manual and antifreeze mode. 
* In manual mode the heaters will turn on if the temperature goes below the starting temperature. You can change this value in the menu. 
* In antifreeze mode heaters will turn on if the temperature goes below 4°C.
* The Chronotermostat has three time window that you can set in the menù. In the automatic mode heaters will turn on if the temperature goes below the starting temperature and the current time is in one of the three time window.


With a long press on the MENU button you can enter the Clock menù. To go to the next option press the MENU button. In this menu you can change Day, Month, Year, Hour and Minute. Use PLUS and MINUS button to select the corret value. Press AUT/MAN to exit. 

Pressing the MENU button you can enter the main menuù. Press the MENU button to go to the next option or press AUT/MAN to exit. In this menu you can:

* Change the starting termperature (in Celsius)
* Choose if the backlight turns off.
* Change the start and end time of the time windows (with a 5 minute precision).

Use PLUS and MINUTE to select the correct option.


